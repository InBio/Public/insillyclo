import os
import sys
from pathlib import Path
import pickle

import pandas as pd
import numpy as np
import math
from collections import OrderedDict

from tkinter import ttk
import tkinter as tk
from PIL import Image, ImageTk
from tkinter import messagebox, simpledialog
from tkinter import filedialog
from tkinter import Tk, Label, PhotoImage

import Moclo_utilities
import Moclo_verification
import Golden_assembly

import Bio
from Bio import SeqUtils, SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.SeqFeature import SeqFeature, FeatureLocation
from Bio.Restriction import AllEnzymes, CommOnly
from Bio.Restriction import *



class InputPart:
    def __init__(self, name, mandatory=False, separable=False, separator=None, interpretable=False, interpretation_parts=None):
        self.input_name = name
        self.input_is_mandatory = mandatory
        self.input_is_separable = separable
        self.input_separator = separator
        self.input_is_interpretable = interpretable
        self.input_interpretation_parts = interpretation_parts if interpretation_parts is not None else ''
        self.input_max_subparts = max(interpretation_parts.keys()) if interpretation_parts is not None else 0

    def display_info(self):
        print(f"Name: {self.input_name}")
        print(f"Mandatory: {self.input_is_mandatory}")
        print(f"Separable: {self.input_is_separable}")
        print(f"Separator: {self.input_separator}")
        print(f"Interpretation: {self.input_is_interpretable}")
        print(f"Interpretation: {self.input_interpretation_parts}")
        print(f"Interpretation_max_subparts: {self.input_max_subparts}")

class AssemblyType:
    def __init__(self, name, backbone=False, backbone_in_output=False, backbone_position=None, separator=None, input_parts=None, interpretatable=False, part_side=False):
        self.assembly_name = name
        self.backbone = backbone
        self.backbone_in_output = backbone_in_output
        self.backbone_position = backbone_position
        self.output_separator = separator
        self.input_parts = input_parts if input_parts is not None else OrderedDict()
        self.output_is_interpretable = interpretatable
        self.output_part_side = part_side

    def display_info(self):
        print(f"Name: {self.assembly_name}")
        print(f"Backbone: {self.backbone}")
        print(f"Separator: {self.output_separator}")
        print(f"Columns: {self.input_parts}")
        print(f"Interpretation: {self.output_is_interpretable}")
        print(f"Part_side: {self.output_part_side}")

    def output_template(self):
        template_data = {'pID': ['pID0001']}
        
        if self.output_is_interpretable:
            template_data['Interpretation'] = ''

        if self.backbone:
            template_data['Backbone'] = ''

        if self.output_part_side:
            template_data['Part_side'] = ''

        for col in self.input_parts:
            print('33')
            print(col)
            template_data[col] = ''
            print('44')

        template_data['Forward_primer'] = ''
        template_data['Reverse_primer'] = ''
        template_data['Restriction_enzyme'] = ''
        
        # Create a DataFrame from the template data
        template_df = pd.DataFrame(template_data).set_index('pID')

        # Save the DataFrame to a CSV file
        template_df.to_csv(f'DB_oP_template_{self.assembly_name}.csv')

        print(f"Template saved to DB_oP_template_{self.assembly_name}.csv")

    def save_assembly(self):
        assemblyfile = self.assembly_name + '.pkl'
        with open(assemblyfile, 'wb') as file:
            pickle.dump(self, file)


class Reactions:
    def __init__(self, DB_oP=None, DB_iP=None,DB_outside=None, DB_primer=None, path_plasmid_repository=None, path_cloning_folder=None, project=None, round=None, save_DB=True, dilution_method='dilutions', volume_enzbuf=3.0, volume_total=10.0,enzyme=None,assembly_type=None,verification=None,verification_PCR=None,verification_Digestion=None):
        self.reaction_DB_oP = DB_oP
        self.reaction_DB_iP = DB_iP
        self.reaction_DB_outside = DB_outside
        self.reaction_DB_primer = DB_primer
        self.reaction_path_plasmid_repository = path_plasmid_repository
        self.reaction_path_cloning_folder = path_cloning_folder
        self.reaction_project = project
        self.reaction_round = round
        self.reaction_save_DB = save_DB
        self.reaction_dilution_method = dilution_method
        self.reaction_volume_enzbuf = volume_enzbuf
        self.reaction_volume_total = volume_total
        self.reaction_enzyme = enzyme
        self.reaction_assembly_type = assembly_type
        self.reaction_verification = verification
        self.reaction_verification_PCR = verification_PCR
        self.reaction_verification_Digestion = verification_Digestion


class ReactionGUI:
    def __init__(self, master):
        self.master = master
        self.reaction_obj = Reactions()
        self.photo = None

        self.create_widgets()

    def create_widgets(self):
        current_folder = os.getcwd()

        

        # Create a vertical scrollbar
        scrollbar = tk.Scrollbar(self.master, orient=tk.VERTICAL)

        # Create a canvas to hold the widgets and attach the scrollbar to it
        canvas = tk.Canvas(self.master, yscrollcommand=scrollbar.set)
        canvas.grid(row=0, column=0, sticky=tk.NSEW)

        # Configure the scrollbar to control the canvas
        scrollbar.config(command=canvas.yview)

        # Create a frame to contain all the widgets
        frame = tk.Frame(canvas, bg='lavenderblush')
        canvas.create_window((0, 0), window=frame, anchor=tk.NW)

        # Add an image to the GUI
        self.add_image(os.path.join(current_folder,'logo_insillyclo.png'),frame)
        
        # SETTING DIRECTORIES
        ttk.Label(frame, text="Setting directories", font=('Helvetica', 15)).grid(row=0, column=0,columnspan=2,sticky='W', pady = 10)
        
        ttk.Button(frame, text="Kill", command=self.to_destroy).grid(row=0, column=2, pady=0)
        
        
        ttk.Label(frame, text="Project:").grid(row=1, column=0,sticky='W')
        self.project = ttk.Entry(frame)
        self.project.insert(tk.END,'Project3')
        self.project.grid(row=1, column=1,sticky='W')

        ttk.Label(frame, text="Round:").grid(row=1, column=2,sticky='W')
        self.round = ttk.Entry(frame)
        self.round.insert(tk.END,'R1')
        self.round.grid(row=1, column=3,sticky='W')

        ttk.Button(frame, text="Update cloning folder", command=self.update_folder_cloning).grid(row=1, column=5, columnspan=1, pady=5,sticky='W')
        
        ttk.Label(frame, text="Cloning folder:").grid(row=2, column=0,sticky='W')
        self.path_cloning_folder = ttk.Entry(frame, width=150)
        self.path_cloning_folder.grid(row=2, column=1, columnspan=6,sticky='W')
        ttk.Button(frame, text="Browse", command=self.browse_cloning_folder).grid(row=2, column=7)

        ttk.Label(frame, text="Plasmid repository:").grid(row=3, column=0,sticky='W')
        self.path_plasmid_repository = ttk.Entry(frame, width=150)
        self.path_plasmid_repository.grid(row=3, column=1, columnspan=6,sticky='W')
        self.path_plasmid_repository.insert(tk.END, os.path.join(current_folder, 'Plasmid_repository')) #### MODIFY HERE DEFAULT PLASMID REPOSITORY
        ttk.Button(frame, text="Browse", command=self.browse_plasmid_repository).grid(row=3, column=7)

        # IMPORTING DATABASES
        ttk.Label(frame, text="Importing databases",font=('Helvetica', 15)).grid(row=4, column=0,columnspan=2,sticky='W', pady = 10)
        
        ttk.Label(frame, text="Output Plasmid DB:").grid(row=5, column=0,sticky='W')
        self.path_DB_oP = ttk.Entry(frame, width=150)
        self.path_DB_oP.grid(row=5, column=1, columnspan=6,sticky='W')
        ttk.Button(frame, text="Browse", command=lambda: self.insert_path(self.path_DB_oP, "csv")).grid(row=5, column=7)

        ttk.Label(frame, text="Input Plasmid DB:").grid(row=6, column=0,sticky='W')
        self.path_DB_iP = ttk.Entry(frame, width=150)
        self.path_DB_iP.grid(row=6, column=1, columnspan=6,sticky='W')
        self.path_DB_iP.insert(tk.END, os.path.join(current_folder,'DB_iP_raw.csv')) #### MODIFY HERE DEFAULT INPUT PLASMID DATABASE
        ttk.Button(frame, text="Browse", command=lambda: self.insert_path(self.path_DB_iP, "csv")).grid(row=6, column=7)

        ttk.Label(frame, text="Outside Plasmid DB:").grid(row=7, column=0,sticky='W')
        self.path_DB_outside = tk.StringVar(value='')
        self.outside= tk.BooleanVar(value=False)
        ttk.Checkbutton(frame, variable=self.outside,command=self.toggle_outside).grid(row=7, column=1,sticky='W')
        self.path_DB_outside = ttk.Entry(frame,state=tk.DISABLED, width=120)
        self.path_DB_outside.grid(row=7, column=2, columnspan=4,sticky='W')
        self.path_DB_outside_button = ttk.Button(frame, text="Browse", command=lambda: self.insert_path(self.path_DB_outside, "csv"),state=tk.DISABLED)
        self.path_DB_outside_button.grid(row=7, column=7,sticky='W')
        
        
        ttk.Label(frame, text="Primer DB:").grid(row=8, column=0,sticky='W')
        self.path_DB_primer = ttk.Entry(frame, width=150)
        self.path_DB_primer.grid(row=8, column=1, columnspan=6,sticky='W')
        self.path_DB_primer.insert(tk.END, os.path.join(current_folder,'DB_primer.csv'))
        ttk.Button(frame, text="Browse", command=lambda: self.insert_path(self.path_DB_primer, "csv")).grid(row=8, column=7)

        ttk.Label(frame, text="Assembly type:").grid(row=9, column=0,sticky='W')
        self.path_assembly_type = ttk.Entry(frame, width=150)
        self.path_assembly_type.grid(row=9, column=1, columnspan=6,sticky='W')
        ttk.Button(frame, text="Browse", command=lambda: self.insert_path(self.path_assembly_type, "pkl")).grid(row=9, column=7)

        ttk.Button(frame, text="Import files", command=lambda: self.import_files(frame)).grid(row=10, column=0, columnspan=2, pady=5)

        # SANITY CHECKS OF INPUT FILES
        



        
        # PARAMETERS AND PREFERENCES
        ttk.Label(frame, text="Parameters and preferences",font=('Helvetica', 15)).grid(row=12, column=0,columnspan=2,sticky='W', pady = 10)
        
        ttk.Label(frame, text="Save_DB:").grid(row=13, column=0,sticky='W')
        self.save_DB = tk.BooleanVar(frame,value=True)
        ttk.Checkbutton(frame, variable=self.save_DB).grid(row=13, column=1,sticky='W')

        ttk.Label(frame, text="Dilution Method:").grid(row=14, column=0,sticky='W')
        self.dilution_method = tk.StringVar(frame,value='dilution')  
        self.dilution_method_combobox = ttk.Combobox(frame, textvariable=self.dilution_method, values=['dilution','one microL'] )
        self.dilution_method_combobox.grid(row=14, column=1, pady=5,sticky='W')


        ttk.Label(frame, text="Volume_enzbuf (µL):").grid(row=15, column=0,sticky='W')
        self.volume_enzbuf = ttk.Entry(frame)
        self.volume_enzbuf.insert(tk.END, "3.0") #### MODIFY HERE DEFAULT VOLUME OF ENZYME AND BUFFER
        self.volume_enzbuf.grid(row=15, column=1,sticky='W')

        ttk.Label(frame, text="Quantity of plasmid in the equimolar mix (fmol) :").grid(row=15, column=3, columnspan = 2,sticky='W')
        self.quantity_fmol_plasmid = ttk.Entry(frame)
        self.quantity_fmol_plasmid.insert(tk.END, "20") #### MODIFY HERE DEFAULT QUANTITY OF INPUT PLASMID
        self.quantity_fmol_plasmid.grid(row=15, column=5,sticky='W')
        
        
        ttk.Label(frame, text="Volume_total (µL) :").grid(row=16, column=0,sticky='W')
        self.volume_total = ttk.Entry(frame)
        self.volume_total.insert(tk.END, "10.0") #### MODIFY HERE DEFAULT TOTAL VOLUME
        self.volume_total.grid(row=16, column=1,sticky='W')

        ttk.Label(frame, text="Volume of part (µL) :").grid(row=16, column=3, columnspan = 2,sticky='W')
        self.set_volume_part = ttk.Entry(frame)
        self.set_volume_part.insert(tk.END, "1") #### MODIFY HERE DEFAULT VOLUME OF PART
        self.set_volume_part.grid(row=16, column=5,sticky='W')
        

        ttk.Label(frame, text="Type IIS enzyme:").grid(row=17, column=0,sticky='W')
        self.enzyme = tk.StringVar(frame,value='BsaI') #### MODIFY HERE DEFAULT ENZYME
        self.enzyme_combobox = ttk.Combobox(frame, textvariable=self.enzyme, values=['BsaI','BsmBI','BbsI','SapI','PaqCI','BpiI'] )
        self.enzyme_combobox.grid(row=17, column=1, pady=5,sticky='W')
        
        
        ttk.Label(frame, text="Assembly verification:").grid(row=18, column=0,sticky='W')
        self.verif = tk.StringVar(frame,value='PCR')  #### MODIFY HERE DEFAULT VERIFICATION
        self.verif_combobox = ttk.Combobox(frame, textvariable=self.verif, values=['PCR','Digestion','Both'])
        self.verif_combobox.grid(row=18, column=1, pady=5,sticky='W')

        self.verif_PCR = tk.StringVar(frame,value=None)
        ttk.Label(frame, text="PCR verification:").grid(row=19, column=0,sticky='W')
        self.verif_PCR = tk.StringVar(frame,value='')  
        self.verif_PCR_combobox = ttk.Combobox(frame, textvariable=self.verif_PCR, values=['DB','Manual'],state='disabled')
        self.verif_PCR_combobox.grid(row=19, column=1, pady=5,sticky='W')

        self.verif_Digest = tk.StringVar(frame,value=None)
        ttk.Label(frame, text="Digestion verification:").grid(row=19, column=2,sticky='W')
        self.verif_Digestion = tk.StringVar(frame,value='')  
        self.verif_Digestion_combobox = ttk.Combobox(frame, textvariable=self.verif_Digestion, values=['DB','Manual'],state='disabled')
        self.verif_Digestion_combobox.grid(row=19, column=3, pady=5,sticky='W')
        
        
        ttk.Button(frame, text="Set verification", command=self.verification).grid(row=18, column=2,sticky='W')


        ttk.Label(frame, text="LESS GOO",font=('Helvetica', 15)).grid(row=20, column=0,columnspan=2,sticky='W', pady = 10)
        
        # MAKE ASSEMBLY
        self.assembly_button = ttk.Button(frame, text="RUN ASSEMBLY", command=lambda: self.make_assembly(frame),state='disabled')
        self.assembly_button.grid(row=21, column=0, columnspan=1, pady=10,sticky='W')
        
        # EXPORT SEQUENCE
        #tk.Button(frame, text="Less goo for sequence expooort", command=lambda: self.compute_sequence(frame)).grid(row=22, column=0, columnspan=2, pady=10,sticky='W')

        # ENTER iP CONCENTRATIONS
        #tk.Button(frame, text="SET iP CONCENTRATION", command=lambda: self.iP_concentration_set(frame)).grid(row=22, column=0, columnspan=1, pady=10,sticky='W')
        
        # CALCULATING iP DILUTIONS
        self.base_row = 25
        self.calcultion_button = ttk.Button(frame, text="COMPUTE iP DILUTION", command=lambda: self.iP_concentration_calculation(frame),state='disabled')
        self.calcultion_button.grid(row=22, column=0, columnspan=1, pady=10,sticky='W')

        # VERIFICATIONS
        self.verification_button = ttk.Button(frame, text="SIMULATE VERIFICATION ", command=lambda: self.verification_simulation(frame),state='disabled')
        self.verification_button.grid(row=(self.base_row + 4), column=0, columnspan=1, pady=10,sticky='W')

        # SAVE DATABASES
        self.saving_button = ttk.Button(frame, text="SAVE DB", command=lambda: self.saving_DB(frame),state='disabled')
        self.saving_button.grid(row=24, column=0, columnspan=1, pady=10,sticky='W')

        # EXPORT Genbank file and update input plasmid database
        self.updating_button = ttk.Button(frame, text="UPDATE DB & REPOSITORY", command=lambda: self.updating_DB(frame),state='disabled')
        self.updating_button.grid(row=26, column=0, columnspan=1, pady=10,sticky='W')

        # Bind the canvas to the scrollbar to allow scrolling
        frame.bind("<Configure>", lambda e: canvas.configure(scrollregion=canvas.bbox("all")))
        
        # Set row and column weights to make the widgets expand with the window size
        self.master.grid_rowconfigure(0, weight=1)
        self.master.grid_columnconfigure(0, weight=1)
        
        # Add this line to place the scrollbar in the master widget
        scrollbar.grid(row=0, column=1, sticky=tk.NS)

        self.style = ttk.Style(self.master)
        # Create a style object from ttk
        

        # Change the theme - replace 'clam' with the desired theme
        self.style.theme_use('vista')
        self.style.configure('TLabel', background='lavenderblush', foreground='black')

    def updating_DB(self,frame):
        #Update DB_iP
        self.DB_oP['pID_final'] = ''
        for oP in self.DB_oP.index:
            print('Taking care of ', oP)
            if self.DB_oP.at[oP,'pID_final_entry']:
                self.DB_oP.at[oP,'pID_final'] = self.DB_oP.at[oP,'pID_final_entry'].get()
            else:
                self.DB_oP.at[oP,'pID_final'] = oP
            print('Adding ', self.DB_oP.at[oP,'pID_final'] ,' to the input plasmid DB.')
            
            self.DB_iP.at[self.DB_oP.at[oP,'pID_final'],'Name'] =  self.DB_oP.at[oP,'Generic_name']
            if self.assembly_type.output_is_interpretable:
                self.DB_iP.at[self.DB_oP.at[oP,'pID_final'],'Interpretation'] =  self.DB_oP.at[oP,'Interpretation']
            
            print('Moving ', self.DB_oP.at[oP,'pID_final'] ,' to the the plasmid repository.')
            
            Moclo_utilities.rename_and_move_genbank(oP,self.DB_oP.at[oP,'pID_final'],self.path_cloning_folder.get(),self.path_plasmid_repository.get())   
            
            saving_path_iP_updated = self.path_cloning_folder.get() + '\\DB_iP_updated.csv'
            self.DB_iP.to_csv(saving_path_iP_updated)
    
        # Assembly    
    def make_assembly(self,frame):

        print('\n')
        print('########     ASSEMBLY      ########')

        count = 1
        self.DB_oP['iP_mix'] = ""
        self.DB_oP['iP_mix_name'] = ""
        self.DB_oP['Sequence'] = ""
        self.DB_oP['Generic_name_long'] = ""
        self.DB_oP['Generic_name_short'] = ""

        self.DB_iP_real = {'pID':[],
              'Concentration':'',
              'Sequence':'',
              'Volume_h2o':'',
              'oP':''}

        self.enzyme_bioR = Moclo_utilities.get_enzyme_by_name(self.enzyme_combobox.get())
        
        for oP in self.DB_oP.index:
            message = 'Assembling ' + oP + ' ('+ str(count) +'/'+ str(len(self.DB_oP.index))+')'
            ttk.Label(frame, text=message).grid(row=21, column=0,columnspan=3)
            self.DB_oP, self.DB_iP_real = Golden_assembly.assembly(
                DB_oP=self.DB_oP,
                DB_iP=self.DB_iP,
                DB_iP_real=self.DB_iP_real,
                oP=oP,
                assembly_type=self.assembly_type,
                oP_seq=True,
                outside_part=list(self.DB_outside.index),
                enzyme=self.enzyme_bioR,
                volume_reaction=float(self.volume_total.get()),
                volume_enzbuf=float(self.volume_enzbuf.get()),
                plasmid_repository=self.path_plasmid_repository.get()
                )
            count +=1
            tk.Label(frame, text='').grid(row=20, column=1)
        self.DB_iP_real = pd.DataFrame(self.DB_iP_real).set_index('pID')

        for iP in self.DB_iP_real.index:
            self.DB_iP_real.at[iP,'oP'] = []
            
        for oP in self.DB_oP.index:
            for iP in self.DB_oP.at[oP,'iP_mix']:
                self.DB_iP_real.at[iP,'oP'].append(oP)
                

        iP_genbank = Moclo_utilities.get_iP_gb(self.path_plasmid_repository.get()) # Load all the plasmids from the input plasmid database
        Features, Features_seq  = Moclo_utilities.get_features(iP_genbank) # Recover all the features from these plasmids
        Moclo_utilities.write_gb(self.DB_oP,Features,Features_seq,self.path_cloning_folder.get()) # Add features to the sequence and write the genbank file in folder_cloning

        ttk.Label(frame, text='iP Dilutions',font=('Helvetica', 12)).grid(row=23,column = 0,sticky='W')
        ttk.Label(frame, text='pID').grid(row=24,column = 0,sticky='W')
        ttk.Label(frame, text='Concentration (ng/µL)').grid(row=24,column = 1)
        row_iP = self.base_row
        self.DB_iP_real['Concentration_tk'] = ''
        for iP in self.DB_iP_real.index:
            ttk.Label(frame, text=iP).grid(row=row_iP,column = 0,sticky='W')
            self.DB_iP_real.at[iP,'Concentration_tk'] = tk.Entry(frame)
            self.DB_iP_real.at[iP,'Concentration_tk'].grid(row=row_iP,column = 1)
            self.DB_iP_real.at[iP,'Concentration_tk'].insert(tk.END,'100')
            row_iP +=1
        row_iP +=1
        ttk.Label(frame, text='H2O volume (µL) in reaction',font=('Helvetica', 12)).grid(row=row_iP,column = 0,sticky='W')
        row_iP +=1
        ttk.Label(frame, text='pID').grid(row=row_iP,column = 0,sticky='W')
        ttk.Label(frame, text='Final pID').grid(row=(row_iP), column=5, columnspan=1,sticky='W')
        self.volume_h2o_oP_label = ttk.Label(frame, text='Volume (µL)')
        self.volume_h2o_oP_label.grid(row=row_iP,column = 1)
        
        row_iP +=1
        self.row_oP = row_iP
    
        for oP in self.DB_oP.index:
            ttk.Label(frame, text=oP).grid(row=row_iP,column = 0,sticky='W')
            self.DB_oP.at[oP,'pID_final_entry'] = ttk.Entry(frame)
            self.DB_oP.at[oP,'pID_final_entry'].insert(tk.END,oP)
            self.DB_oP.at[oP,'pID_final_entry'].grid(row=row_iP, column=5, columnspan=1, pady=1,sticky='W')
            
            row_iP +=1

        if (self.verif_combobox.get() == 'Digestion') or (self.verif_combobox.get() == 'Both'):
            if self.verif_Digestion_combobox.get() == 'Manual':
                self.DB_oP['Restriction_enzyme_entry'] =''
                row_oP = self.row_oP
                self.res_enzyme_label = ttk.Label(frame, text='Restriction enzyme(s)')
                self.res_enzyme_label.grid(row=(row_oP-1),column = 2,sticky='W')
                for oP in self.DB_oP.index:
                    self.DB_oP.at[oP,'Restriction_enzyme_entry'] = ttk.Entry(frame)
                    self.DB_oP.at[oP,'Restriction_enzyme_entry'].insert(tk.END,'NotI')
                    self.DB_oP.at[oP,'Restriction_enzyme_entry'].grid(row=row_oP, column=2,sticky='W')
                    row_oP +=1

                #self.Digestion_button = ttk.Button(frame, text="Save Enzyme")#, command=lambda: self.saving_enzyme(frame))
                #self.Digestion_button.grid(row=row_oP, column=2, columnspan=1, pady=5,sticky='W')


        if (self.verif_combobox.get() == 'PCR') or (self.verif_combobox.get() == 'Both'):
            if self.verif_PCR_combobox.get() == 'Manual':
                self.DB_oP['Forward_primer_entry'] =''
                self.DB_oP['Reverse_primer_entry'] =''    
                row_oP = self.row_oP
                self.forward_primer_label = ttk.Label(frame, text='Forward primer')
                self.forward_primer_label.grid(row=(row_oP-1),column = 3,sticky='W')
                self.reverse_primer_label = ttk.Label(frame, text='Reverse primer')
                self.reverse_primer_label.grid(row=(row_oP-1),column = 4,sticky='W')
                
                for oP in self.DB_oP.index:
                    self.DB_oP.at[oP,'Forward_primer_entry'] = ttk.Entry(frame)
                    self.DB_oP.at[oP,'Forward_primer_entry'].insert(tk.END,'For')
                    self.DB_oP.at[oP,'Forward_primer_entry'].grid(row=row_oP, column=3,sticky='W')
                    
                    self.DB_oP.at[oP,'Reverse_primer_entry'] = ttk.Entry(frame)
                    self.DB_oP.at[oP,'Reverse_primer_entry'].insert(tk.END,'Rev')
                    self.DB_oP.at[oP,'Reverse_primer_entry'].grid(row=row_oP, column=4,sticky='W')
                    
                    row_oP +=1
        
            
        
        diff = row_iP - self.base_row
        self.base_row_2 = self.base_row + diff
        self.verification_button.grid(row=(self.base_row_2 + 4), column=0, columnspan=2, pady=10)
        self.saving_button.grid(row=(self.base_row_2 + 7), column=0, columnspan=2, pady=10)
        self.updating_button.grid(row=(self.base_row_2 + 9), column=0, columnspan=2, pady=10)
    
    
    ## not used
    #def compute_sequence(self,frame):
        #iP_genbank = Moclo_utilities.get_iP(os.getcwd() + "\\Plasmids_gb_ID\\") # Load all the plasmids from the input plasmid database
        #Features, Features_seq  = Moclo_utilities.get_features(iP_genbank) # Recover all the features from these plasmids
        #Moclo_utilities.write_gb(self.DB_oP,Features,Features_seq,self.path_cloning_folder.get()) # Add features to the sequence and write the genbank file in folder_cloning

    
    def verification_simulation(self,frame):
        gels_verification = True
        marker = [250,500,750,1000,1500, 2000, 2500, 3000, 3500, 4000, 5000, 6000, 8000, 10000] # DNA marker for gel simulation
        marker_bold = [1000, 3000, 6000] # Band of the marker that should be in bold

        print('\n')
        print('########     VERIFICATION      ########')

        if (self.verif_combobox.get() == 'PCR') or (self.verif_combobox.get() == 'Both'):
            if self.verif_PCR_combobox.get() == 'Manual':
                self.DB_oP['Forward_primer'] =''
                self.DB_oP['Reverse_primer'] =''    
                for oP in self.DB_oP.index:
                    self.DB_oP.at[oP,'Forward_primer'] = str(self.DB_oP.at[oP,'Forward_primer_entry'].get())
                    self.DB_oP.at[oP,'Reverse_primer'] = str(self.DB_oP.at[oP,'Reverse_primer_entry'].get())
                    
        if (self.verif_combobox.get() == 'Digestion') or (self.verif_combobox.get() == 'Both'):
            if self.verif_Digestion_combobox.get() == 'Manual':
                self.DB_oP['Restriction_enzyme']=''
                for oP in self.DB_oP.index:
                    self.DB_oP.at[oP,'Restriction_enzyme']= (self.DB_oP.at[oP,'Restriction_enzyme_entry'].get()).split('_')
                
            self.DB_oP['Restriction_enzyme_BioR']=''
            for oP in self.DB_oP.index:
                self.DB_oP.at[oP,'Restriction_enzyme_BioR'] = []
                for enz_str in self.DB_oP.at[oP,'Restriction_enzyme']:
                    self.DB_oP.at[oP,'Restriction_enzyme_BioR'].append(Moclo_utilities.get_enzyme_by_name(enz_str))

        if self.verif_combobox.get() == 'PCR':
            self.DB_oP = Moclo_verification.PCR(self.DB_oP,self.DB_primer)
            if gels_verification == True:
                Moclo_verification.PCR_agarose_gel(self.DB_oP,self.path_cloning_folder.get(),marker,marker_bold)    
        elif self.verif_combobox.get() == 'Digestion':
            self.DB_oP = Moclo_verification.Digestion(self.DB_oP)
            if gels_verification == True:
                Moclo_verification.Digestion_agarose_gel(self.DB_oP,self.path_cloning_folder.get(),marker,marker_bold)
        else:
            self.DB_oP = Moclo_verification.PCR(self.DB_oP,self.DB_primer)
            self.DB_oP = Moclo_verification.Digestion(self.DB_oP)
            if gels_verification == True:
                Moclo_verification.PCR_agarose_gel(self.DB_oP,self.path_cloning_folder.get(),marker,marker_bold)
                Moclo_verification.Digestion_agarose_gel(self.DB_oP,self.path_cloning_folder.get(),marker,marker_bold)


    def clean_iP(self,frame):
        base_row = self.base_row
        for iP in self.DB_iP_real.index:
            print(iP)
            print(self.DB_iP_real.at[iP,'Volume_part_tk'])
            if self.DB_iP_real.at[iP,'Volume_part_tk']:
                self.DB_iP_real.at[iP,'Volume_part_tk'].destroy()
                self.DB_iP_real.at[iP,'Volume_h2o_tk'].destroy()
                self.DB_iP_real.at[iP,'Volume_part_tk'] = None
                self.DB_iP_real.at[iP,'Volume_h2o_tk'] = None

        if self.volume_part: 
            self.volume_part.destroy()
            self.volume_part = None
            self.volume_h2o.destroy()
            self.volume_h2o = None
        
        for oP in self.DB_oP.index:
            if self.DB_oP.at[oP,'Volume_h2o_entry']:
                self.DB_oP.at[oP,'Volume_h2o_entry'].destroy()
                self.DB_oP.at[oP,'Volume_h2o_entry'] = None

            if 'pID_final_entry' in list(self.DB_oP.keys()):
                if self.DB_oP.at[oP,'pID_final_entry']:
                    self.DB_oP.at[oP,'pID_final_entry'].destroy()
                    self.DB_oP.at[oP,'pID_final_entry'] = None
                
            if 'Restriction_enzyme_entry' in list(self.DB_oP.keys()):
                if self.DB_oP.at[oP,'Restriction_enzyme_entry']:
                    self.DB_oP.at[oP,'Restriction_enzyme_entry'].destroy()
                    self.DB_oP.at[oP,'Restriction_enzyme_entry'] = None

            if 'Forward_primer_entry' in list(self.DB_oP.keys()):
                if self.DB_oP.at[oP,'Forward_primer_entry']:        
                    self.DB_oP.at[oP,'Forward_primer_entry'].destroy()
                    self.DB_oP.at[oP,'Reverse_primer_entry'].destroy()
                    self.DB_oP.at[oP,'Forward_primer_entry'] = None
                    self.DB_oP.at[oP,'Reverse_primer_entry'] = None
        
        if self.volume_h2o_oP_label:
            self.volume_h2o_oP_label.destroy()
            self.volume_h2o_oP_label = None
        
        if 'Restriction_enzyme_entry' in list(self.DB_oP.keys()):
            self.res_enzyme_label.destroy()
            self.res_enzyme_label = None

        if 'Forward_primer_entry' in list(self.DB_oP.keys()):
            self.forward_primer_label.destroy()
            self.reverse_primer_label.destroy()
            self.forward_primer_label = None
            self.reverse_primer_label = None

        self.clean_button.destroy()
                    
        
    def iP_concentration_calculation(self,frame):

        print('\n')
        print('#######     CONCENTRATION CALCULATION     ########')
        
        self.DB_iP_real['Sequence'] = ''
        self.DB_iP_real['Volume_part'] = ''
        self.DB_iP_real['Volume_part_dilution'] = ''
        self.DB_iP_real['Volume_h2o'] = ''
        self.DB_iP_real['Volume_part_tk'] = ''
        self.DB_iP_real['Volume_h2o_tk'] = ''
        
        volume_part = float(self.set_volume_part.get())
        conc_final = float(self.quantity_fmol_plasmid.get()) / (10**6) #0.00002 # nanomol/microL (= 20 femtomol/microL)
        
        row_iP = self.base_row
        self.volume_part = ttk.Label(frame, text='Volume part (µL)')
        self.volume_part.grid(row=24,column = 2)
        self.volume_h2o = ttk.Label(frame, text='Volume H2O (µL)')
        self.volume_h2o.grid(row=24,column = 3)
        self.clean_button = ttk.Button(frame, text="RESET", command=lambda: self.clean_iP(frame))
        self.clean_button.grid(row = 22, column = 3)
        self.dilutions = True
        
        print('\n~~~~     Volume input plasmid      ~~~~')
        for iP in self.DB_iP_real.index:
            self.DB_iP_real.at[iP,'Concentration'] = float(self.DB_iP_real.at[iP,'Concentration_tk'].get())
            self.DB_iP_real = Moclo_utilities.volume_iP(iP, self.DB_iP_real,self.path_plasmid_repository.get(), volume_part, conc_final,self.dilution_method_combobox.get(),float(self.volume_total.get()))
            self.DB_iP_real.at[iP,'Volume_part_tk'] = ttk.Label(frame, text=str(round(self.DB_iP_real.at[iP,'Volume_part_dilution'],2)))
            self.DB_iP_real.at[iP,'Volume_part_tk'].grid(row=row_iP,column = 2)
            self.DB_iP_real.at[iP,'Volume_h2o_tk'] = ttk.Label(frame, text=str(round(self.DB_iP_real.at[iP,'Volume_h2o'],2)))
            self.DB_iP_real.at[iP,'Volume_h2o_tk'].grid(row=row_iP,column = 3)
            row_iP +=1
        row_iP +=3
        print('\n~~~~     Volume H2O     ~~~~')
        
        self.DB_oP['Volume_h2o_entry'] = ''
        for oP in self.DB_oP.index:
            self.DB_oP = Moclo_utilities.volume_h2o(oP, self.DB_oP, self.DB_iP_real,float(self.volume_total.get()),float(self.volume_enzbuf.get()))
            self.DB_oP.at[oP,'Volume_h2o_entry'] = ttk.Label(frame, text=str(round(self.DB_oP.at[oP,'Volume_h2o'],2)))
            self.DB_oP.at[oP,'Volume_h2o_entry'].grid(row=row_iP,column = 1)
            row_iP +=1
   
    def saving_DB(self,frame):
        
        saving_path_oP = self.path_cloning_folder.get() + '\\DB_oP_' + self.round.get() + '_final.csv'
        columns_DB_oP_to_exclude = ['pID_final_entry', 'Restriction_enzyme_entry', 'Volume_h2o_entry']
        DB_oP_saving = self.DB_oP.copy()
        for col in columns_DB_oP_to_exclude:
            if col in list(DB_oP_saving.keys()):
                DB_oP_saving = DB_oP_saving.drop(columns=col)
        DB_oP_saving.to_csv(saving_path_oP)    
        
        saving_path_iP_real = self.path_cloning_folder.get() + '\\DB_iP_real_' + self.round.get() + '.csv'
        columns_DB_iP_to_exclude = ['Concentration_tk', 'Volume_part_tk', 'Volume_h2o_tk']
        DB_iP_saving = self.DB_iP_real.drop(columns=columns_DB_iP_to_exclude)
        
        if self.dilution_method_combobox.get() == 'dilution':
            DB_iP_saving = DB_iP_saving.drop(columns=['Volume_part_dilution','Volume_h2o'])

    
        DB_iP_saving.to_csv(saving_path_iP_real)
    
        
        
    #def iP_concentration_set(self,frame):
        #tk.Label(frame, text='pID').grid(row=24,column = 0)
        #tk.Label(frame, text='Concentration (ng/µL)').grid(row=24,column = 1)
        #row_iP = self.base_row
        #self.DB_iP_real['Concentration_tk'] = ''
        #for iP in self.DB_iP_real.index:
        #    tk.Label(frame, text=iP).grid(row=row_iP,column = 0)
        #    self.DB_iP_real.at[iP,'Concentration_tk'] = tk.Entry(frame)
        #    self.DB_iP_real.at[iP,'Concentration_tk'].grid(row=row_iP,column = 1)
        #    self.DB_iP_real.at[iP,'Concentration_tk'].insert(tk.END,'100')
        #    row_iP +=1
        #diff = row_iP - self.base_row
        #self.base_row_2 = self.base_row + diff
        #self.calcultion_button.grid(row=(self.base_row_2), column=0, columnspan=2, pady=10)
        #self.verification_button.grid(row=(self.base_row_2 + 4), column=0, columnspan=2, pady=10)
        #self.saving_button.grid(row=(self.base_row_2 + 7), column=0, columnspan=2, pady=10)

    
    def verification(self):
        verif = self.verif.get()
        if (verif=='PCR'):
            self.verif_Digestion_combobox.set('')
            self.verif_PCR_combobox.set('DB')
            self.verif_PCR_combobox.config(state='readonly')
            self.verif_Digestion_combobox.config(state='disabled')
            
        elif (verif=='Digestion'):
            self.verif_Digestion_combobox.set('DB')
            self.verif_PCR_combobox.set('')
            self.verif_PCR_combobox.config(state='disabled')
            self.verif_Digestion_combobox.config(state='readonly')
            self.verif_PCR = tk.StringVar(self.master,value='')
        else :
            self.verif_Digestion_combobox.set('DB')
            self.verif_PCR_combobox.set('DB')
            self.verif_PCR_combobox.config(state='readonly')
            self.verif_Digestion_combobox.config(state='readonly')

    
    def toggle_outside(self):
        if self.outside.get():
            self.path_DB_outside.config(state=tk.NORMAL)
            self.path_DB_outside_button.config(state=tk.NORMAL)
        else:
            self.path_DB_outside.delete(0, tk.END)
            self.path_DB_outside.config(state=tk.DISABLED)
            self.path_DB_outside_button.config(state=tk.DISABLED)

    def add_image(self, image_path, frame, max_width=100, max_height=100):
        # Open the image using Pillow (PIL)
        try:
            pil_image = Image.open(image_path)
        except Exception as e:
            print(f"Error loading image: {e}")
            return

        # Resize the image
        pil_image.thumbnail((max_width, max_height), Image.LANCZOS)

        # Convert the resized image to a PhotoImage object
        try:
            self.photo = ImageTk.PhotoImage(pil_image)
        except Exception as e:
            print(f"Error creating PhotoImage: {e}")
            return

        # Create a label to display the resized image
        image_label = Label(frame, image=self.photo)
        image_label.grid(row=0, column=4, padx=0, pady=0)

        # Keep a reference to the image to prevent garbage collection
        image_label.photo = self.photo
        
    
    def update_folder_cloning(self):
        # Enable or disable 'backbone_in_output' and 'backbone_position' based on the state of 'backbone'
        current_folder = os.getcwd()
        self.path_cloning_folder.delete(0,tk.END) 
        self.path_cloning_folder.insert(tk.END, os.path.join(current_folder,self.project.get(),self.round.get()))
        self.path_DB_oP.delete(0,tk.END) 
        self.path_DB_oP.insert(tk.END, os.path.join(current_folder,self.project.get(),self.round.get(),'DB_oP_'+self.round.get()+'.csv'))

    def import_files(self,frame):
        print('Importing DB_oP')
        self.DB_oP = pd.read_csv(self.path_DB_oP.get(),sep=';').set_index('pID')
        self.DB_oP.fillna(value=np.nan, inplace=True)
        self.DB_oP = self.DB_oP.where(pd.notna(self.DB_oP), "")
        ttk.Label(frame, text="imported").grid(row=5, column=9)
        for oP in self.DB_oP.index:
            self.DB_oP.at[oP,'Restriction_enzyme'] = str(self.DB_oP.at[oP,'Restriction_enzyme']).split('_') 
        print('Done\nImporting DB_iP')
        self.DB_iP = pd.read_csv(self.path_DB_iP.get(),sep=';').set_index('pID')
        ttk.Label(frame, text="imported").grid(row=6, column=9)
        if self.path_DB_outside.get() == '':
            DB_outside_temp = {'pID':['pDOESNOTEXIST'],
                              'Name':'DOESTNOTEXIST'}
            self.DB_outside=pd.DataFrame(DB_outside_temp).set_index('pID')
        else:
            print('Done\nImporting DB_outside')
            self.DB_outside = pd.read_csv(self.path_DB_outside.get(),sep=';').set_index('pID')
            ttk.Label(frame, text="imported").grid(row=7, column=9)
        print('Done\nImporting DB_primer')
        self.DB_primer = pd.read_csv(self.path_DB_primer.get(),sep=';').set_index('Name')
        for primer in self.DB_primer.index:
            self.DB_primer.at[primer,'Oligo sequence'] = str(self.DB_primer.at[primer,'Oligo sequence']).replace(" ", "")
            self.DB_primer.at[primer,'Oligo sequence'] = str(self.DB_primer.at[primer,'Oligo sequence']).upper()
        ttk.Label(frame, text="imported").grid(row=8, column=9)
        print('Done\nImporting assembly type')
        with open(self.path_assembly_type.get(), 'rb') as file:
        # Load the pickled data
            self.assembly_type = pickle.load(file)
        ttk.Label(frame, text="imported").grid(row=9, column=9)
        print('Done')
        self.assembly_button.config(state='normal')
        self.calcultion_button.config(state='normal')
        self.verification_button.config(state='normal')
        self.saving_button.config(state='normal')
        self.updating_button.config(state='normal')
 
    def browse_plasmid_repository(self):
        path = filedialog.askdirectory(title="Select Directory")
        self.path_plasmid_repository.delete(0, tk.END)
        self.path_plasmid_repository.insert(tk.END, path)

    def browse_cloning_folder(self):
        path = filedialog.askdirectory(title="Select Directory")
        self.path_cloning_folder.delete(0, tk.END)
        self.path_cloning_folder.insert(tk.END, path)

    def insert_path(self, entry, file_type):
        path = filedialog.askopenfilename(title=f"Select {file_type} file", filetypes=[(f"{file_type} files", f"*.{file_type}")])
        entry.delete(0, tk.END)
        entry.insert(tk.END, path)

    def create_reaction_object(self):
        self.reaction_obj = Reactions(
            path_DB_oP=self.DB_oP.get(),
            path_DB_iP=self.DB_iP.get(),
            path_DB_outside=self.DB_outside.get(),
            path_DB_primer=self.DB_primer.get(),
            path_plasmid_repository=self.path_plasmid_repository.get(),
            path_cloning_folder=self.path_cloning_folder.get(),
            project=self.project.get(),
            round=self.round.get(),
            save_DB=self.save_DB.get(),
            dilution_method=self.dilution_method.get(),
            volume_enzbuf=float(self.volume_enzbuf.get()),
            volume_total=float(self.volume_total.get()),
            enzyme=self.enzyme.get(),
            assembly_type=self.assembly_type.get(),
            verification=self.verif.get(),
            verification_PCR=self.verif_PCR.get(),
            verification_Digestion=self.verif_Digestion.get()
        )

        # Display the created Reactions object
        print("Reactions Object Created:")
        print(vars(self.reaction_obj))
    
    def to_destroy(self):
        self.master.destroy()
        

def main():
    root = tk.Tk()
    root.title("Assembly Simulator")
    root.state('zoomed')

    # Create ReactionGUI object
    reaction_gui = ReactionGUI(root)

    # Run the Tkinter event loop
    root.mainloop()

# Example usage
if __name__ == "__main__":
    main()
