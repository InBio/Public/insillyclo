import pandas as pd
import pickle
import sys
import numpy as np
from tkinter import ttk
import tkinter as tk
from PIL import Image, ImageTk
from tkinter import messagebox, simpledialog
from collections import OrderedDict
import os


class InputPart:
    def __init__(self, name, mandatory=False, separable=False, separator=None, interpretable=False, interpretation_parts=None):
        self.input_name = name
        self.input_is_mandatory = mandatory
        self.input_is_separable = separable
        self.input_separator = separator
        self.input_is_interpretable = interpretable
        self.input_interpretation_parts = interpretation_parts if interpretation_parts is not None else ''
        self.input_max_subparts = max(interpretation_parts.keys()) if interpretation_parts is not None else 0

    def display_info(self):
        print(f"Name: {self.input_name}")
        print(f"Mandatory: {self.input_is_mandatory}")
        print(f"Separable: {self.input_is_separable}")
        print(f"Separator: {self.input_separator}")
        print(f"Interpretation: {self.input_is_interpretable}")
        print(f"Interpretation: {self.input_interpretation_parts}")
        print(f"Interpretation_max_subparts: {self.input_max_subparts}")

class AssemblyType:
    def __init__(self, name, backbone=False, backbone_in_output=False, backbone_position=None, separator='', input_parts=None, interpretatable=False, part_side=False):
        self.assembly_name = name
        self.backbone = backbone
        self.backbone_in_output = backbone_in_output
        self.backbone_position = backbone_position
        self.output_separator = separator
        self.input_parts = input_parts if input_parts is not None else OrderedDict()
        self.output_is_interpretable = interpretatable
        self.output_part_side = part_side

    def display_info(self):
        print(f"Name: {self.assembly_name}")
        print(f"Backbone: {self.backbone}")
        print(f"Separator: {self.output_separator}")
        print(f"Columns: {self.input_parts}")
        print(f"Interpretation: {self.output_is_interpretable}")
        print(f"Part_side: {self.output_part_side}")

    def output_template(self):
        template_data = {'pID': ['pID0001']}
        
        if self.output_is_interpretable:
            template_data['Interpretation'] = ''

        if self.backbone:
            template_data['Backbone'] = ''

        if self.output_part_side:
            template_data['Part_side'] = ''

        for col in self.input_parts:
            template_data[col] = ''


        template_data['Forward_primer'] = ''
        template_data['Reverse_primer'] = ''
        template_data['Restriction_enzyme'] = ''
        
        # Create a DataFrame from the template data
        template_df = pd.DataFrame(template_data).set_index('pID')

        # Save the DataFrame to a CSV file
        template_df.to_csv(f'DB_oP_template_{self.assembly_name}.csv')

        print(f"Template saved to DB_oP_template_{self.assembly_name}.csv")

    def save_assembly(self):
        assemblyfile = self.assembly_name + '.pkl'
        with open(assemblyfile, 'wb') as file:
            pickle.dump(self, file)

class InputPartGUI:
    def __init__(self, master, part_number):
        self.master = master
        self.part_number = part_number
        self.input_part = InputPart(name=f"InputPart_{part_number}")

        
        self.create_widgets()

    def create_widgets(self):
        # Label and entry for input part name
        label = ttk.Label(self.master, text=f"Input Part {self.part_number} Properties", font=('Helvetica', 14))
        label.grid(row=0, column=0, columnspan=2, pady=10)

        ttk.Label(self.master, text="Name:", font=('Helvetica', 12)).grid(row=1, column=0, pady=5,padx=15,sticky='W')
        self.name_entry = ttk.Entry(self.master, font=('Helvetica', 12))
        self.name_entry.grid(row=1, column=1, pady=5)

        # Checkbox for mandatory property
        ttk.Label(self.master, text="Mandatory:", font=('Helvetica', 12)).grid(row=2, column=0, pady=5,padx=15,sticky='W')
        self.mandatory_var = tk.BooleanVar(value=False)
        ttk.Checkbutton(self.master, variable=self.mandatory_var).grid(row=2, column=1, pady=5)

        # Checkbox for separable property
        ttk.Label(self.master, text="Separable:", font=('Helvetica', 12)).grid(row=3, column=0, pady=5,padx=15,sticky='W')
        self.separable_var = tk.BooleanVar(value=False)
        self.separable_checkbox = ttk.Checkbutton(self.master, variable=self.separable_var, command=self.toggle_separator_entry)
        self.separable_checkbox.grid(row=3, column=1, pady=5)

        # Entry for separator
        ttk.Label(self.master, text="Separator:", font=('Helvetica', 12)).grid(row=3, column=2, pady=5,padx=15,sticky='W')
        self.separator_entry = ttk.Entry(self.master, font=('Helvetica', 12), state=tk.DISABLED)
        self.separator_entry.grid(row=3, column=3, pady=5)

        # Checkbox for interpretable property
        ttk.Label(self.master, text="Interpretable:", font=('Helvetica', 12)).grid(row=4, column=0, pady=5,padx=15,sticky='W')
        self.interpretable_var = tk.BooleanVar(value=False)
        self.interpretable_checkbox = ttk.Checkbutton(self.master, variable=self.interpretable_var, command=self.toggle_interpretation_entries)
        self.interpretable_checkbox.grid(row=4, column=1, pady=5)

        # Combobox for number of interpretations
        ttk.Label(self.master, text="Number of Interpretations:", font=('Helvetica', 12)).grid(row=4, column=2, pady=5)
        self.num_interpretations_var = tk.IntVar(value=1)
        self.num_interpretations_combobox = ttk.Combobox(self.master, textvariable=self.num_interpretations_var, state='disabled', values=[1, 2, 3, 4, 5, 6])
        self.num_interpretations_combobox.grid(row=4, column=3, pady=5)

        # Button to add interpretations
        ttk.Label(self.master, text="Add Interpretation:", font=('Helvetica', 12)).grid(row=6, column=0, pady=5,padx=15,sticky='W')
        self.add_interpretation_button = ttk.Button(self.master, text="Add Interpretation", command=self.add_interpretations)
        self.add_interpretation_button.grid(row=6, column=1, pady=5)

        # Variable to track interpretation added status
        self.interpretation_added = tk.BooleanVar(value=False)

        
        # Button to save input part
        ttk.Button(self.master, text="Save Input Part", command=self.save_input_part).grid(row=8, column=0, columnspan=2, pady=10)
        
    def toggle_interpretation_entries(self):
        # Enable or disable 'Number of Interpretations' entry based on the state of 'Interpretable'
        if self.interpretable_var.get():
            self.num_interpretations_combobox.config(state='readonly')
        else:
            self.num_interpretations_combobox.set(0)
            self.num_interpretations_combobox.config(state='disabled')

    def toggle_separator_entry(self):
        # Enable or disable 'Separator' based on the state of 'Separable'
        if self.separable_var.get():
            self.separator_entry.config(state=tk.NORMAL)
        else:
            self.separator_entry.config(state=tk.DISABLED)

            
    def add_interpretations(self):
        
        # Get the number of interpretations selected
        num_interpretations = int(self.num_interpretations_var.get())
        interpretations = {}

        # Create a new window for entering interpretations
        interpretation_window = tk.Toplevel(self.master)
        interpretation_window.title(f"Enter {num_interpretations} Interpretations")

        for i in range(1, num_interpretations + 1):
            # Label for interpretation
            label = ttk.Label(interpretation_window, text=f"Interpretation {i}:", font=('Helvetica', 12))
            label.grid(row=i - 1, column=0, pady=5)

            # Entry fields for interpretation values
            entries = []
            for j in range(i):
                entry = ttk.Entry(interpretation_window, font=('Helvetica', 12))
                entry.grid(row=i - 1, column=j + 1, pady=5)
                entries.append(entry)

            interpretations[i] = entries

        # Button to save interpretations
        save_button = ttk.Button(interpretation_window, text="Save Interpretations", command=lambda: self.save_interpretations(interpretation_window, interpretations))
        save_button.grid(row=num_interpretations, column=0, columnspan=num_interpretations + 1, pady=10)

        self.interpretation_added.set(True)

        # Disable the "Add Interpretation" button
        self.add_interpretation_button.config(state=tk.DISABLED)

    def save_interpretations(self, interpretation_window, interpretations):
        # Retrieve interpretations from entry fields
        interpretations_data = {}
        for i, entry_list in interpretations.items():
            interpretation_values = [entry.get() for entry in entry_list]
            interpretations_data[i] = interpretation_values

        # Set the interpretations in the input part
        self.input_part.input_interpretation_parts = interpretations_data
        print(f"Interpretations added: {self.input_part.input_interpretation_parts}")

        # Close the interpretation window
        interpretation_window.destroy()

    
    def save_input_part(self):
        # Get values from the entry widgets and checkboxes
        self.input_part.input_name = self.name_entry.get()
        self.input_part.input_is_mandatory = self.mandatory_var.get()
        self.input_part.input_is_separable = self.separable_var.get()
        self.input_part.input_separator = self.separator_entry.get() or None  # Set to None if nothing is entered
        self.input_part.input_is_interpretable = self.interpretable_var.get()

        # Display information about the input part
        self.input_part.display_info()
        print('\n')

        # Close the input part window
        self.master.destroy()

class AssemblyTypeGUI:
    def __init__(self, master):
        self.master = master
        self.assembly_type = AssemblyType(name="")

        self.input_part_gui_list = []
        self.current_part_number = 1

        self.create_widgets()

    def create_widgets(self):
        # Label and entry for assembly type name
        ttk.Label(self.master, text="Assembly Properties", font=('Helvetica', 14)).grid(row=0, column=0, columnspan=2, pady=10, padx = 10,sticky = 'W')

        ttk.Label(self.master, text="Name:", font=('Helvetica', 12)).grid(row=1, column=0, pady=5, padx = 10,sticky = 'W')
        self.name_entry = ttk.Entry(self.master, font=('Helvetica', 12))
        self.name_entry.grid(row=1, column=1, pady=5)

        # Checkbox for backbone property
        ttk.Label(self.master, text="Backbone:", font=('Helvetica', 12)).grid(row=2, column=0, pady=5, padx = 10,sticky = 'W')
        self.backbone_var = tk.BooleanVar(value=False)
        self.backbone_checkbox = ttk.Checkbutton(self.master, variable=self.backbone_var, command=self.toggle_backbone_entries)
        self.backbone_checkbox.grid(row=2, column=1, pady=5,sticky = 'W')

        # Checkbox for backbone in output property
        ttk.Label(self.master, text="Backbone in Output:", font=('Helvetica', 12)).grid(row=2, column=2, pady=5, padx = 10)
        self.backbone_in_output_var = tk.BooleanVar(value=False)
        self.backbone_in_output_checkbox = ttk.Checkbutton(self.master, variable=self.backbone_in_output_var, state=tk.DISABLED,command=self.toggle_backbone_in_output_entries )
        self.backbone_in_output_checkbox.grid(row=2, column=3, pady=5,sticky = 'W')
        
        # Set Default Value for backbone position
        ttk.Label(self.master, text="Backbone Position:", font=('Helvetica', 12)).grid(row=2, column=4, pady=5, padx = 10)
        self.backbone_position_var = tk.StringVar(value='')  
        self.backbone_position_combobox = ttk.Combobox(self.master, textvariable=self.backbone_position_var, state='disabled', values=['','Start','End'] )
        self.backbone_position_combobox.grid(row=2, column=5, pady=5)
            
        # Entry for output separator
        ttk.Label(self.master, text="Output Separator:", font=('Helvetica', 12)).grid(row=3, column=0, pady=5, padx = 10,sticky = 'W')
        self.output_separator_entry = ttk.Entry(self.master, font=('Helvetica', 12))
        self.output_separator_entry.grid(row=3, column=1, pady=5)

        # Checkbox for interpretable property
        ttk.Label(self.master, text="Interpretable:", font=('Helvetica', 12)).grid(row=4, column=0, pady=5, padx = 10,sticky = 'W')
        self.interpretable_var = tk.BooleanVar(value=False)
        ttk.Checkbutton(self.master, variable=self.interpretable_var).grid(row=4, column=1, pady=5,sticky = 'W')

        # Dropdown for part side
        ttk.Label(self.master, text="Part Side:", font=('Helvetica', 12)).grid(row=5, column=0, pady=5, padx = 10,sticky = 'W')
        self.part_side_var = tk.BooleanVar(value=False)
        ttk.Checkbutton(self.master, variable=self.part_side_var).grid(row=5, column=1, pady=5,sticky = 'W')

        # Button to add input part
        ttk.Button(self.master, text="Add Input Part", command=self.add_input_part).grid(row=6, column=0, columnspan=2, pady=10, padx = 10,sticky = 'W')
        # Button to save assembly type
        ttk.Button(self.master, text="Save Assembly Type", command=self.save_assembly_type).grid(row=7, column=0, columnspan=2, pady=10, padx = 10,sticky = 'W')

    def toggle_backbone_entries(self):
        # Enable or disable 'backbone_in_output' and 'backbone_position' based on the state of 'backbone'
        if self.backbone_var.get():
            self.backbone_in_output_checkbox.config(state=tk.NORMAL)
        else:
            self.backbone_in_output_checkbox.config(state=tk.DISABLED)


    def toggle_backbone_in_output_entries(self):
        # Enable or disable 'backbone_position' based on the state of 'backbone_in_output'
        if self.backbone_in_output_var.get():
            self.backbone_position_combobox.config(state='readonly')
        else:
            self.backbone_position_combobox.set('')
            self.backbone_position_combobox.config(state='disabled')
        
    def add_input_part(self):
        # Create a new InputPartGUI instance when the button is clicked
        part_gui = InputPartGUI(tk.Toplevel(self.master), self.current_part_number)
        self.input_part_gui_list.append(part_gui)
        self.current_part_number += 1

    def save_assembly_type(self):
        # Get values from the entry widgets and checkboxes
        self.assembly_type.assembly_name = self.name_entry.get()
        self.assembly_type.backbone = self.backbone_var.get()
        self.assembly_type.backbone_in_output = self.backbone_in_output_var.get()
        self.assembly_type.backbone_position = self.backbone_position_var.get() or None  # Set to None if nothing is entered
        self.assembly_type.output_separator = self.output_separator_entry.get() or ''  # Set to None if nothing is entered
        self.assembly_type.output_is_interpretable = self.interpretable_var.get()
        self.assembly_type.output_part_side = self.part_side_var.get()

        # Create a dictionary to store input parts
        input_parts = {}
        for part_gui in self.input_part_gui_list:
            part = part_gui.input_part
            input_parts[part.input_name] = part
            part.display_info()

        # Set the input parts in the assembly type
        self.assembly_type.input_parts = input_parts
        self.assembly_type.display_info()

        # Now you can use self.assembly_type as needed, e.g., save it to a file
        self.assembly_type.output_template()
        self.assembly_type.save_assembly()
        messagebox.showinfo("Success", "Assembly Type Saved")

        # Print information about the saved AssemblyType
        print("\nAssemblyType Information:")
        self.assembly_type.display_info()

        # Output details to a text file
        output_file = self.assembly_type.assembly_name + "_details.txt"
        
        with open(output_file, "w") as f:
            f.write("Assembly Type Details:\n")
            f.write(f"Name: {self.assembly_type.assembly_name}\n")
            f.write(f"Backbone: {self.assembly_type.backbone}\n")
            f.write(f"Backbone in Output: {self.assembly_type.backbone_in_output}\n")
            f.write(f"Backbone Position: {self.assembly_type.backbone_position}\n")
            f.write(f"Output Separator: {self.assembly_type.output_separator}\n")
            f.write(f"Interpretable: {self.assembly_type.output_is_interpretable}\n")
            f.write(f"Part Side: {self.assembly_type.output_part_side}\n")

            f.write("\nInput Parts:\n")
            for part_gui in self.input_part_gui_list:
                part = part_gui.input_part
                f.write(f"\nInput Part {part.input_name} Details:\n")
                f.write(f"Name: {part.input_name}\n")
                f.write(f"Mandatory: {part.input_is_mandatory}\n")
                f.write(f"Separable: {part.input_is_separable}\n")
                f.write(f"Separator: {part.input_separator}\n")
                f.write(f"Interpretable: {part.input_is_interpretable}\n")
                f.write(f"Interpretation Parts: {part.input_interpretation_parts}\n")
                f.write(f"Max Subparts: {part.input_max_subparts}\n")

        print(f"Assembly details saved to {output_file}")

        # Close the assembly type window
        self.master.destroy()

def main():
    root = tk.Tk()
    root.title("Assembly Designer")
    root.geometry("900x400")  # Set your desired window size
    app = AssemblyTypeGUI(root)
    root.mainloop()

if __name__ == "__main__":
    main()