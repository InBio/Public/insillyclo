import pandas as pd
import numpy as np
import math
import Bio
from Bio import SeqUtils, SeqIO
from Bio.Seq import Seq
import os
from Bio.SeqRecord import SeqRecord
from Bio.SeqFeature import SeqFeature, FeatureLocation
from Bio.Restriction import AllEnzymes, CommOnly
from Bio import Restriction
import shutil

def define_level(level_output):
    try:
        level_output in ['L1','B','L2']
    except:
        print('Invalid level_output, should be "L1", "L2" or "B".')
        sys.exit(1)
        
    if level_output == 'L1':
        level_input = 'L0'
        restriction_enzyme = 'BsaI'
        sheet = 'TU'
    elif level_output == 'B':
        level_input = 'L0'
        restriction_enzyme = 'BsaI'
        sheet = 'Raw'
    elif level_output == 'Raw':
        level_input = 'Raw'
        restriction_enzyme = ''
        while restriction_enzyme not in  ['BsaI','BsmbI','BbsI','SapI']:
            print('Enzymes supported are BsaI, BsmBI, SapI and BbsI.')
            restriction_enzyme = input(str('What restriction enzyme are you using?'))
        sheet = 'Raw'
    else:
        level_input = 'L1'
        restriction_enzyme = 'BsmBI'
        sheet = 'multiTU'
    return (level_input, restriction_enzyme, sheet)

def volume_iP(iP, DB_iP_real,folder_genbank, volume_part, conc_final, method, volume_reaction): #here volume_part not used because 1 micr liter is taken instead for the dilution
    print('-----')
    print(iP, 'loading')
    file_pl = os.path.join(folder_genbank,iP+'.gb')
    for seq_record in SeqIO.parse(file_pl, "genbank"):
        DB_iP_real.at[iP,'Sequence'] = seq_record.seq
        DB_iP_real.at[iP,'Molecular_weight'] = Bio.SeqUtils.molecular_weight(seq_record.seq,seq_type='DNA', double_stranded=True, circular=True)      
    conc_temp = DB_iP_real.at[iP,'Concentration'] / DB_iP_real.at[iP,'Molecular_weight'] # = nanomol / microLiter
    
    if method == 'one microL':
        DB_iP_real.at[iP,'Volume_h2o'] = round((1 * conc_temp / conc_final) - volume_part,2) # microLiter
        DB_iP_real.at[iP,'Volume_part'] = volume_part
        DB_iP_real.at[iP,'Volume_part_dilution'] = 1
    elif method == 'dilution':
        DB_iP_real.at[iP,'Volume_h2o'] = 0
        DB_iP_real.at[iP,'Volume_part'] = (conc_final/10) * volume_reaction / conc_temp
        DB_iP_real.at[iP,'Volume_part_dilution'] = (conc_final/10) * volume_reaction / conc_temp
    else:
        print('Error of method, please enter one microL of dilution')
        
    print('processed')
    return (DB_iP_real)

def volume_h2o(oP, DB_oP,DB_iP_real, volume_reaction,volume_enzbuf):
    print('-----')
    print(oP, 'loading')
    vol_temp = volume_reaction - volume_enzbuf
    for iP in DB_oP.at[oP,'iP_mix']:
        vol_temp = vol_temp - DB_iP_real.at[iP,'Volume_part']
    DB_oP.at[oP,'Volume_h2o'] = round(vol_temp,3)
    print('processed')
    return (DB_oP)

def get_iP_gb(folder_path):
    existing_records=[]
    for filename in os.listdir(folder_path):
        if filename.endswith(".gb"):
            filepath = os.path.join(folder_path, filename)
            record = SeqIO.read(filepath, "genbank")
            existing_records.append(record)
    return existing_records

def get_features(existing_records):
    Features_seq = {}
    Features = {}
    for existing_record in existing_records:
        for feature in existing_record.features:
            if 'label' in feature.qualifiers.keys():
                if feature.qualifiers['label'][0] not in Features.keys():
                    Features[feature.qualifiers['label'][0]] = feature
                    Features_seq[feature.qualifiers['label'][0]] = feature.location.extract(existing_record).seq
    return Features,Features_seq

def write_gb(DB_oP,Features,Features_seq ,filepath):
    for oP in DB_oP.index:
        record = SeqRecord(seq=Seq(DB_oP.at[oP,'Sequence']),
                id=oP,
                name=oP,
                description = str(DB_oP.at[oP,'iP_mix']))
        record.annotations["molecule_type"] = "DNA"
        for feature in Features.keys():
            if Features_seq[feature] in record.seq:               
                start_position = str(record.seq).find(str(Features_seq[feature]))
                feature_location = FeatureLocation(start=start_position, end=start_position + len(Features_seq[feature]), strand=1)
                new_feature = SeqFeature(location=feature_location, type=Features[feature].type)
                new_feature.qualifiers["label"] = feature
                record.features.append(new_feature)  # Copy feature

        print(oP,'.gb written')
        output_file = filepath +'\\' + oP +'.gb'
        with open(output_file, "w") as output_handle:
            SeqIO.write(record, output_file, "genbank")

def check_DB_oP(DB_oP,level_output,version):
    # Check that columns are correct
    mandatory_columns_oP = {'L1':['pID','Assembly_type','Backbone','Type_2','Type_3','Type_4'],
                       'L2':['pID','Assembly_type','Backbone','TU1','TU2','TU3','TU4'],
                       'Raw':['pID','Assembly_type','Backbone','Part_1'],}
    for col in mandatory_columns_oP[level_output]:
        if col not in DB_oP.keys():
            print('Missing column '+ col +'in DB_oP_'+version+'.xslx.')


    # Check if primers are to be added in DB or manually
    if ('Primer_for' not in DB_oP.keys()) or ('Primer_rev' not in DB_oP.keys()):
        print('Lack 1 or 2 columns for primers in DB_oP_'+version.split('\\')[0]+'.xslx, make sure you add the primer manually after.')
            
    
def check_DB_primer(DB_primer,version):
    # checking  DB_primer
    mandatory_columns_primers = ['Name','Oligo sequence']
    for col in mandatory_columns_primers:
        if col not in DB_primer.keys():
            print('Missing column '+ col +'in DB_primer.xslx.')

def rename_and_move_genbank(oP, new_oP, folder_cloning,plasmid_repository):
    # Check if the file exists
    original_path = folder_cloning+ '\\' +oP+'.gb'
    print(original_path)
    if not os.path.isfile(original_path):
        print(f"Error: File '{original_path}' not found.")
        return
        
    # Create the new file path with the desired name and extension
    new_path = os.path.join(plasmid_repository, new_oP+'.gb')

    try:
        # Rename the file
        #os.replace(original_path, new_path)
        shutil.copy(original_path, new_path)
        print(f"File '{oP}' successfully move to '{new_oP}' in new the repository.")

    except Exception as e:
        print(f"Error moving file file: {e}")


def get_enzyme_by_name(name):
    all_enzymes = {enzyme.__name__: enzyme for enzyme in AllEnzymes}
    comm_enzymes = {enzyme.__name__: enzyme for enzyme in CommOnly}

    # Combine dictionaries for all and common enzymes
    enzyme_dict = {**all_enzymes, **comm_enzymes}
    
    return enzyme_dict.get(name, None)




def check_nb_subparts(nb_subparts,part_name,nb_max_subparts):
    try:
        if nb_subparts > input_max_subparts:
            raise ValueError('Too many subparts in part: ', part_name)
    except ValueError as ve:
        print('Error : ',ve)
        sys.exit(1)
        




 