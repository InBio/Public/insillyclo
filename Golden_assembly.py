import pandas as pd
import numpy as np
import math
import Bio
from Bio import SeqUtils, SeqIO
from Bio.Seq import Seq
import os
from Bio.SeqRecord import SeqRecord
from Bio.SeqFeature import SeqFeature, FeatureLocation
from Bio.Restriction import AllEnzymes, CommOnly
from Bio import Restriction
import shutil
from pathlib import Path
import sys
import random

def assembly(DB_oP, DB_iP,DB_iP_real, oP, assembly_type, oP_seq, enzyme='BsaI', outside_part = None,volume_reaction=10,volume_enzbuf=3,plasmid_repository=''):

    print(oP, ' ~~ Assembly  ~~\n')

    assembly_mix ={}
    
    ### 1. get the mix of parts
    for input_part in assembly_type.input_parts.values():
        part_name = DB_oP.at[oP,input_part.input_name]
        if part_name =='':
            check_mandatory(input_part) # if empty and mandatory raise an error
        else:
            #print(part_name, input_part.input_interpretation_parts)
            iPs = get_iP(part_name, input_part,DB_iP)
            if len(iPs.keys()) == 1:
                is_file_present(plasmid_repository=plasmid_repository, filename= (list(iPs.keys())[0]+'.gb'))
            else:
                for iP in iPs.keys():
                    is_file_present(plasmid_repository=plasmid_repository, filename= (iP+'.gb'))
            assembly_mix.update(iPs)
            
    if assembly_type.backbone == True:
        backbone = DB_oP.at[oP,'Backbone']
        if backbone in DB_iP.index:
            backbone_pID = backbone
            backbone_name = DB_iP.at[backbone,'Name']
        elif backbone in DB_iP['Name']:
            backbone_pID = DB_iP.index[DB_iP['Name'] == backbone].values[0]
            backbone_name = backbone
        else:
            backbone_pID = backbone
            backbone_name = backbone
            print(backbone, ' not found in DB_iP, added anyway.')
        assembly_mix[backbone_pID] = backbone_name
    else:
        backbone_name = ''

    print('assembly mix for ',oP,': ',assembly_mix)
    #print('assembly mix name for ',oP,': ',assembly_mix_name)
    DB_oP.at[oP,'iP_mix'] = list(assembly_mix.keys())
    DB_oP.at[oP,'iP_mix_name'] = assembly_mix
    
    DB_oP.at[oP,'Volume_h2o'] = volume_reaction - volume_enzbuf - len(DB_oP.at[oP,'iP_mix'])

    ### 2. Add the plasmid ID of used input plasmids to DB_iP_real if it is not in it already
    for iP in DB_oP.at[oP,'iP_mix']:
        if iP not in DB_iP_real['pID']:
            DB_iP_real['pID'].append(iP)
    
    ### 3. Compute the sequence of the output plasmids with the function compute_oPseq
    if oP_seq == True:
        DB_oP.at[oP,'Sequence'] = compute_oPseq(oP=oP, DB_oP=DB_oP, enzyme=enzyme, outside_part=outside_part, plasmid_repository=plasmid_repository) ### MIGHT NEED TO BE CHANGED

    ### 4.Compute generic names
    exluded_part = []
    DB_oP.at[oP,'Generic_name'] = compute_oPname(oP,DB_oP,backbone_name,assembly_type,exluded_part)
    #DB_oP.at[oP,'Generic_name'] = compute_oPname(oP,DB_oP,assembly_type,exluded_part) #set excloded part as empty to keep all the parts
        
    return (DB_oP,DB_iP_real)


def get_iP(part_name,input_part,DB_iP):
    part_name_temp = {}
    iPs = {}
    

    # A. Get the list of parts/subparts
    if input_part.input_is_separable:
        part_name = part_name.split(input_part.input_separator)
        
    else:
        part_name = [part_name]

    # B. Create dictionnar with each subpart and its interpretation
    if input_part.input_is_interpretable:
        for count,subpart_name in enumerate(part_name):
            part_name_temp[subpart_name] = input_part.input_interpretation_parts[len(part_name)][count]
    else:
        for count,subpart_name in enumerate(part_name):
            part_name_temp[subpart_name] = ''
    
    
    # C. Look for if the corresponding ID in the DB_iP
    for name,interpretation in part_name_temp.items():
        if interpretation == '':
            selected_row = DB_iP[DB_iP['Name'] == name]
        else :
            selected_row = DB_iP[(DB_iP['Name'] == name) & (DB_iP['Interpretation'] == interpretation)]
        try:
            if selected_row.shape[0] == 1:
                iPs[selected_row.index.values[0]] = name
            elif selected_row.shape[0] == 0:
                if name in DB_iP.index:
                    iPs[name] = name
                    print(name, ' found as a pID directly.')
                else:
                    raise ValueError(part_name, ' not found in DB_iP.')
            else :
                raise ValueError(part_name,' ',interpretation, ' present several times in DB_iP.')
        except ValueError as ve:
            print('\n Oops - InSillyClo ERROR : ', ve ,'\n')
            sys.exit(1)
    print(input_part.input_name,' : ',iPs)
    return iPs




def compute_oPname(oP,DB_oP,backbone_name,assembly_type,exluded_part): #maybe DB_oP not needed but give only iP_mix_name and subtype
    generic_name = ''
    iP_mix_name = list(DB_oP.at[oP,'iP_mix_name'].values())
    #print(iP_mix_name)

    if assembly_type.output_is_interpretable:
        interpretation = DB_oP.at[oP,'Interpretation'] 
    else:
        interpretation = None
        
    #if assembly_type.output_is_separable:
        #separator = assembly_type.output_separator
    #else:
        #separator = ''
    
    if assembly_type.backbone == True:
        if backbone_name in iP_mix_name:
            iP_mix_name.remove(backbone_name)
        else:
            print(backbone_name,' not in the iP_mix_name')
            backbone = ''
        
    for part_name in iP_mix_name:
        if part_name not in exluded_part:
            generic_name = generic_name + assembly_type.output_separator + part_name

    if assembly_type.output_separator != '':
        generic_name = generic_name[1:]
    
    if (assembly_type.backbone_in_output == True) & (backbone_name != ''):
        if(assembly_type.backbone_position == 'start') or (assembly_type.backbone_position == 'Start') :
            generic_name = backbone_name + assembly_type.output_separator + generic_name
        elif (assembly_type.backbone_position == 'end') or (assembly_type.backbone_position == 'End') :
            generic_name = generic_name + assembly_type.output_separator + backbone_name
        else :
            print(oP,': backbone name not put in generic name')

    return generic_name
        

def compute_oPseq(oP, DB_oP, enzyme,outside_part,plasmid_repository): 
    # assembly parts contains the DNA fragments inside and outside the 2 cutting sites for each input plasmid of one assembly, and the overhangs left by restriction enzyme
    iP_mix = DB_oP.at[oP,'iP_mix']
    assembly_parts = pd.DataFrame({'pID':iP_mix,
                                       'Part_sens':'',
                                       '5_sens':'', # contain all the 5'overhang in sens
                                       'Part_antisens':'',
                                       '5_antisens':'', # contain all the 5'overhang in antisens
                                       'Outside':'',
                                       'Used':[False]*len(iP_mix)}).set_index('pID')
    marker_sens_backbone = str('tgagaaagcgccacgcttcccgaagggagaaaggcggacaggtatccggt').upper() #seq of ColE1 ori to know the orientation of the bakcbone, and use the correct sens for the assembly


    # 1.1 Digestion of input plasmids and recovery of the fragments
    # loop in iP_mix to fill assembly_parts for each plasmid of the mix
    print('\n1.1 Restriction digestion of input plasmids :')
    count = 0
    for count,iP in enumerate(iP_mix): 
        
        file_pl = plasmid_repository+'\\'+ iP+'.gb' #recover the plasmid sequence
    
        print('Digesting '+ iP)
        if iP in outside_part:
            part_position = 'outside'
            print('Taking oustide part for ',iP)
        else:
            part_position = 'inside'
        
        for seq_record in SeqIO.parse(file_pl, "genbank"):
            part_sens, part_antisens, outside = get_fragments(seq_record.seq, enzyme,part_position) #use the get_fragments function to do the digestion and recover part and outside fragments
            assembly_parts.at[iP,'Part_sens'] = part_sens
            assembly_parts.at[iP,'5_sens'] = part_sens[0:4] # get overhang 5'
            assembly_parts.at[iP,'Part_antisens'] = part_antisens
            assembly_parts.at[iP,'5_antisens'] = part_antisens[0:4] # get overhang 5' in antisens
            assembly_parts.at[iP,'Outside'] = outside

        # initiate the assembly with the backbone, sequence of the outpute plasmid is stored in output_sequence
        if 'Backbone' in list(DB_oP.keys()):
            initialization_message = 'Initialization of the assembly with the backbone ' + iP
            initialization_iP = iP
            if (iP in list(DB_oP['Backbone'])): 
                assembly_backbone = iP
                assembly_parts.at[iP,'Used'] = True
                if marker_sens_backbone in part_sens:  
                    output_sequence = part_sens[4:]
                    print('Backbone in correct sens.')
                elif marker_sens_backbone in part_antisens:
                    output_sequence = part_antisens[4:]
                    print('Backbone in antisens.')
                else:
                    output_sequence = part_sens
                    print('marker_sens_backbone not found.') # to allow assembly in case the backbone does not contain this ori
        else:
            if count == 0:
                initialization_message = 'Initialization of the assembly with ' + iP
                initialization_iP = iP
                assembly_parts.at[iP,'Used'] = True
                output_sequence = part_sens[4:]
        count += 1
                
    print('\n1.2 Assembly of digested fragments into oP :')
    print(initialization_message)
                
    # 1.2 Assembly of the the fragments    
    # Take the 4 bp nucleotides in 3' of the output_sequence and look for the input plasmid with the matching 5'overhang
    
    for idx in range(0,len(iP_mix)-1):    
        overhang_3 = output_sequence[-4:]  # get last 4 nucleotides in 3' 
        
        if overhang_3 in list(assembly_parts['5_sens']):  
            iP = assembly_parts.index[assembly_parts['5_sens'] == overhang_3].values[0] # get the plasmids whose 5' 4 nucleotides are the same
            output_sequence = output_sequence + assembly_parts.at[iP,'Part_sens'][4:]
            assembly_parts.at[iP,'Used'] = True
            #print(overhang_3,' as 5\' in ',iP,' in sens')
            
        elif overhang_3 in list(assembly_parts['5_antisens']): 
            iP = assembly_parts.index[assembly_parts['5_antisens'] == overhang_3].values[0] # get the plasmids whose 5' 4 nucleotides are the same 
            output_sequence = output_sequence + assembly_parts.at[iP,'Part_antisens'][4:]
            assembly_parts.at[iP,'Used'] = True
            #print(overhang_3,' as 5\' in ',iP,' in antisens')
            
        else:
            
            print('\n Oops - InSillyClo ERROR : No correct overhang found (looking for ', overhang_3 ,", to match with the 3' of " , initialization_iP,') \n')
            print("\n5' overhangs of the input plasmids:")
            print(assembly_parts['5_sens'])
            print("\n3' overhangs of the input plasmids:")
            print(assembly_parts['5_antisens'])
            break
            
        print(iP, ' added to the assembly')
        initialization_iP = iP
            
    if list(assembly_parts['Used']) == [True]* len(iP_mix):
        print('All input plasmids were used, assembly correct')
        print('-----------------------------------------------\n')
    else:
        print('\n Oops - InSillyClo ERROR : Not all imput plasmids used, wrong assembly\n')
        print('Assembled sequence before the error happened:')
        print(output_sequence)
        print('-----------------------------------------------\n')
        

    return output_sequence



def get_fragments(seq,enzyme,part_position = 'inside'):
    if enzyme == 'BsaI':
        site_for = 'GGTCTC'
        site_rev = 'GAGACC'
        inter_s = 1
    elif enzyme == 'BsmBI':
        site_for = 'CGTCTC'
        site_rev = 'GAGACG'
        inter_s = 1
    elif enzyme == 'BbsI':
        site_for = 'GAAGAC'
        site_rev = 'GTCTTC'
        inter_s = 2
    elif enzyme == 'SapI':
        site_for = 'GCTCTTC'
        site_rev = 'GAAGAGC'
        inter_s = 1
    else:
        site_for = enzyme.site.upper()
        site_rev = str(Seq(site_for).reverse_complement()).upper()
        inter_s =  enzyme.fst5 - len(site_for)


    count = 0
    
    while (site_for not in seq) or (site_rev not in seq): #just moving the sequence in case a cutting site is "cut" between the end and the beginning
        shift = random.randint(30,60)
        seq = seq[shift:] + seq[:shift]
        count += 1
        try:
            if count == 100:
                raise ValueError('Missing one or two enzyme binding sites not found in the sequence')
        except ValueError as ve:
            print('\n Oops - InSillyClo ERROR : ', ve ,'\n')
            print('Enzyme site : ', site_for)
            print('Sequence : ',seq)
            sys.exit(1)
            
    nb_cut_for = len(seq.split(site_for)) - 1
    nb_cut_rev = len(seq.split(site_rev)) - 1
    nb_cut_total = nb_cut_for + nb_cut_rev
    try:
        if (nb_cut_for + nb_cut_rev) !=2:
            raise ValueError('Wrong number of enzyme binding site, ' + str(nb_cut_total) +' sites were found.')
    except ValueError as ve:
            print('\n Oops - InSillyClo ERROR : ', ve ,'\n')
            sys.exit(1)
        
            
    
    
    #reorder the sequence to have PART-site_rev-BACKBONE-site_for
    part_1 = seq.split(site_for)[0] + site_for 
    part_2 = seq.split(site_for)[1]
    seq = part_2 + part_1
    
    
    part_outside = site_rev + seq.split(site_rev)[1] # take site_rev-BACKBONE-site_for
    part_inside = seq.split(site_rev)[0] # take PART

    part_outside_sens = str(part_inside[-(4+inter_s):] + part_outside + part_inside[:(4+inter_s)]).upper() #add the nucleotides between the enzyme site and the cut site
    part_inside_sens = str(part_inside[inter_s:-inter_s]).upper() #remove the nucleotides between the enzyme site and the cut site
    part_inside_antisens = str(Seq(part_inside_sens).reverse_complement()).upper()
    part_outside_antisens =str(Seq(part_outside_sens).reverse_complement()).upper()
    if part_position == 'inside':
        return(part_inside_sens,part_inside_antisens,part_outside_sens)
    else : 
        return(part_outside_antisens,part_outside_sens ,part_inside_sens)



def is_file_present(plasmid_repository, filename):
    file_path = Path(plasmid_repository) / filename

    try:
        if not file_path.is_file():
            raise ValueError(filename, ' not present in plasmid repository.')
    except ValueError as ve:
        print('\n Oops - InSillyClo ERROR : ', ve ,'\n')
        sys.exit(1)

def check_mandatory(part):
        try:
            if part.input_is_mandatory == True :
                raise ValueError('Missing the mandatory part ', part.input_name, ' in DB_oP.')
            else:
                print('Warning : Missing the NON mandatory part ', part.input_name, ' in DB_oP.')
        except ValueError as ve:
            print('\n Oops - InSillyClo ERROR : ', ve ,'\n')
            sys.exit(1)

# Deprecated
# raise an error the name of iP is not found in DB_iP
def test_iP(part_name,DB_iP):
    try:
        if part_name not in list(DB_iP['Name']):
                raise ValueError(part_name +' not in DB_iP')
    except ValueError as ve:
            print('\n Oops - InSillyClo ERROR : ', ve ,'\n') 
            sys.exit(1)
    
                