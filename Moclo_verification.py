import pandas as pd
import numpy as np
import math
import Bio
from Bio import SeqUtils, SeqIO
from Bio.Seq import Seq
import os
import random
from Bio import Restriction
import matplotlib.pyplot as plt

def PCR(DB_oP,DB_primer):
    DB_oP['PCR_amplification'] = ''
    for oP in DB_oP.index:
        print('PCR for ', oP)
        primer_for_input = DB_oP.at[oP,'Forward_primer']
        primer_rev_input = DB_oP.at[oP,'Reverse_primer']
        try:
            if primer_for_input not in list(DB_primer.index):
                if primer_for_input == '':
                    raise ValueError('Missing forward primer')
                else:
                    raise ValueError('Forward primer not found in DB_primer')
        except ValueError as ve:
            print('\n Oops - InSillyClo ERROR : ',ve,'\n\n')
        try:
            if primer_rev_input not in list(DB_primer.index):
                if primer_rev_input == '':
                    raise ValueError('Missing reverse primer')
                else:
                    raise ValueError('Reverse primer not found in DB_primer')
        except ValueError as ve:
            print('\n Oops - InSillyClo ERROR : ',ve,'\n\n') 
        
        DB_oP.at[oP,'PCR_amplification'] = PCR_simulation(DB_oP.at[oP,'Sequence'], DB_primer.at[primer_for_input,'Oligo sequence'],DB_primer.at[primer_rev_input,'Oligo sequence'])
        DB_oP.at[oP,'Length PCR amplification'] = len(DB_oP.at[oP,'PCR_amplification'])
    return DB_oP


def PCR_simulation(seq_oP, primer_for, primer_rev):
    primer_for = Seq(primer_for)    
    primer_rev_RC = Seq(primer_rev).reverse_complement()
    count = 0
    while (str(primer_for) not in seq_oP) or (str(primer_rev_RC) not in seq_oP): #just moving the sequence in case a cutting site is "cut" between the end and the beginning
        shift = random.randint(30,60)
        seq_oP = seq_oP[shift:] + seq_oP[:shift]
        count += 1
        try:
            if count == 100:
                raise ValueError('Binding sites of 1 or 2 primers not found in the sequence')
        except ValueError as ve:
            print('\n Oops - InSillyClo ERROR : ',ve,'\n') 


    part_1 = seq_oP.split(str(primer_for))[0] + primer_for
    part_2 = seq_oP.split(str(primer_for))[1]
    seq_oP = part_2 + part_1
    
    seq_oP = Seq(seq_oP)
    
    pos_rev = seq_oP.find(primer_rev_RC)
    
    PCR_frag = str(primer_for) + seq_oP[:pos_rev + len(primer_rev)]
    return(str(PCR_frag))

def PCR_agarose_gel(DB_oP,folder_cloning, marker = [250,500,750,1000,1500, 2000, 2500, 3000, 3500, 4000, 5000, 6000, 8000, 10000], marker_bold =[1000, 3000, 6000]):
    plt.figure(figsize = (len(DB_oP.index) +2,5))
    for m in marker:
        if m in marker_bold:
            wi = 3
        else:
            wi = 1 
        plt.text(-1, (m - m/20), str(m),fontsize='small')       
        plt.plot([0,1],[m,m],color ='black', lw = wi)
        
    len_min = np.min(DB_oP['Length PCR amplification'])
    len_max = np.max(DB_oP['Length PCR amplification'])
    len_min = np.min([100,len_min - 50])
    len_max = np.max([12000,len_max + 2000])
    plt.ylim(len_min,len_max)
    
    count = 1.5
    for oP in DB_oP.index:
        y = [DB_oP.at[oP,'Length PCR amplification'],DB_oP.at[oP,'Length PCR amplification']]
        x = [count , count+1]
        plt.plot(x,y, color = 'black')
        plt.text(count+0.3, y[0] + y[0]/20, int(y[0]), fontsize = 'xx-small')
        plt.text(count-0.2, len_min + 10, oP,fontsize='small')       
        count = count + 1.5
    
    plt.xlim(-1.6,count) 
    plt.text(-1, len_min + 10, 'PCR',color='mediumblue')         
    plt.yscale('log')
    plt.xticks([])
    plt.yticks([])
    plt.axis('off')
    plt.savefig(folder_cloning +'\\PCR_gel_.png')
    plt.plot()

def Digestion(DB_oP):
    DB_oP['Digested_fragments'] = ''
    DB_oP['Length_digested_fragments'] = ''
    for oP in DB_oP.index:
        res_enzymes = DB_oP.at[oP,'Restriction_enzyme_BioR']
        print('Digestion of ', oP,' with enzymes ',res_enzymes)
        fragments, lengths = Digestion_simulation(DB_oP.at[oP,'Sequence'],res_enzymes)
        DB_oP.at[oP,'Length_digested_fragments'] = lengths
        DB_oP.at[oP,'Digested_fragments'] = fragments
    return DB_oP

def Digestion_simulation(seq_oP,res_enzymes):
        
    # Cut with the first enzyme, obtain n1 fragments
    try:
        if res_enzymes == [None]:
            raise ValueError('Restriction enzyme for verification not found. It is missing or you made a typo in the spelling.\nGuidelines : First letter should be a capital, use capital "i" at the end (and not "1"), and separate enzymes with "_". ')
    except ValueError as ve:
        print('\n Oops - InSillyClo ERROR : ',ve,'\n\n')
    
    fragments = [Seq(seq_oP)]
        
    lin = False
    for enzyme in res_enzymes:
        
        if len(enzyme.search(Seq(seq_oP), linear = False)) == 0:
            print('InSillyClo Warning : ',enzyme, ' not cutting.')
            
        fragments_temp = fragments
        fragments = []
        
        for fragment_temp in fragments_temp:
            cut_sites = enzyme.search(fragment_temp, linear = lin)
            #print(enzyme,len(cut_sites))
            if (len(cut_sites) == 0) & lin ==True:
                fragments.append(fragment_temp)
            else:
                for cut in range(len(cut_sites)):
                    if lin == False :
                        if cut == 0:
                            fragments.append(Seq(fragment_temp[cut_sites[len(cut_sites) - 1]:] + fragment_temp[:cut_sites[cut]]))
                        else:
                            fragments.append(Seq(fragment_temp[cut_sites[cut-1]:cut_sites[cut]]))
                    else:
                        if cut == 0:
                            fragments.append(Seq(fragment_temp[:cut_sites[cut]]))
                            if len(cut_sites) == 1:
                                fragments.append(Seq(fragment_temp[cut_sites[cut]:]))    
                        elif cut == (len(cut_sites)-1):
                            fragments.append(Seq(fragment_temp[cut_sites[cut-1]:cut_sites[cut]]))
                            fragments.append(Seq(fragment_temp[cut_sites[cut]:]))
                        else:
                            fragments.append(Seq(fragment_temp[cut_sites[cut-1]:cut_sites[cut]]))
        #print(enzyme,len(fragments))
        lin = True
        
    length_fragments = []
    for fragment in fragments:
        length_fragments.append(len(fragment))

    return fragments,length_fragments

                
def Digestion_agarose_gel(DB_oP,folder_cloning, marker = [250,500,750,1000,1500, 2000, 2500, 3000, 3500, 4000, 5000, 6000, 8000, 10000], marker_bold =[1000, 3000, 6000]):
    plt.figure(figsize = (len(DB_oP.index) +2,5))
    for m in marker:
        if m in marker_bold:
            wi = 3
        else:
            wi = 1 
        plt.text(-1, (m - m/20), str(m),fontsize='small')       
        plt.plot([0,1],[m,m],color ='black', lw = wi)
    length_all = []
    for lengths in DB_oP['Length_digested_fragments']:
        length_all += lengths
    len_min = np.min(length_all)
    len_max = np.max(length_all)
    len_min = np.min([100,len_min - 50])
    len_max = np.max([12000,len_max + 2000])
    plt.ylim(len_min,len_max)
    
    count = 1.5
    for oP in DB_oP.index:
        init = 0
        for frag in DB_oP.at[oP,'Length_digested_fragments']:
            y = [frag,frag]
            x = [count , count+1]
            if init == 0:
                plt.plot(x,y, label = oP, color = 'black')
                plt.text(count+0.3, y[0] + y[0]/20, str(y[0]), fontsize = 'xx-small')
                plt.text(count+0.1, len_min + 10, oP,fontsize='small')   
                init=1
            else:
                plt.text(count+0.3, y[0] + y[0]/20, str(y[0]), fontsize = 'xx-small')
                plt.plot(x,y, color = 'black')
                
        count = count + 1 + 0.5
        
    plt.text(-1, len_min + 10, 'Digestion',color='forestgreen')          
    plt.yscale('log')
    plt.xticks([])   
    plt.yticks([])
    plt.axis('off')
    plt.savefig(folder_cloning+'\\Digestion_gel_.png')
    plt.plot()


def PCR_old(DB_oP,DB_primer,primer_choice = 'Manual'):
    DB_oP['PCR_amplification'] = ''
    for oP in DB_oP.index:
        print('PCR for ', oP)
        if primer_choice == 'DB':
            primer_for_input = DB_oP.at[oP,'Forward_primer']
            primer_rev_input = DB_oP.at[oP,'Reverse_primer']
            DB_oP.at[oP,'PCR_amplification'] = PCR_simulation(DB_oP.at[oP,'Sequence'], DB_primer.at[primer_for_input,'Oligo sequence'],DB_primer.at[primer_rev_input,'Oligo sequence'])
        elif primer_choice == 'Manual':
            print('Choose primer pair for ',oP)
            primer_for_input = str(input('Forward : '))
            primer_rev_input = str(input('Reverse : '))
            DB_oP.at[oP,'PCR_amplification'] = PCR_simulation(DB_oP.at[oP,'Sequence'], DB_primer.at[primer_for_input,'Oligo sequence'],DB_primer.at[primer_rev_input,'Oligo sequence'])
            DB_oP.at[oP,'Forward_primer'] = primer_for_input
            DB_oP.at[oP,'Reverse_primer'] = primer_rev_input
        DB_oP.at[oP,'Length PCR amplification'] = len(DB_oP.at[oP,'PCR_amplification'])
    return DB_oP